..
    :copyright: Copyright (c) 2014 Martin Pengelly-Phillips
    :license: See LICENSE.txt.

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 1.0.1
    :date: 2021-03-30

    .. change: changed
        :tags: setup

        Lower minimum version for clique module compatible to py3k.

    .. change: changed
        :tags: setup

        Lower minimum version for clique module compatibleto py3k.

    .. change:: new
       :tags: API
        
        :meth:`riffle.model.Item.modified` now returns the timestamp itself
        where it used to return a string representation. The value still may
        also be None.

    .. change:: new
       :tags: interface

        Sorting by modified time will now by chronological instead of
        lexicographical.

.. release:: 1.0.0 
    :date: 2020-10-13

    .. change:: new
       :tags: API

        Update code to Python3.X, drop support for Python2.X

    .. change:: new
       :tags: interface

        Update code to PySide2, drop support for Pyside.

.. release:: 0.3.0
    :date: 2016-07-19

    .. change:: new
        :tags: interface

        :kbd:`Backspace` key now navigates up a level in the browser.

.. release:: 0.2.1
    :date: 2016-07-19

    .. change:: fixed
        :tags: documentation

        Documentation fails to build on Read the Docs due to PySide dependency.

.. release:: 0.2.0
    :date: 2016-07-18

    .. change:: fixed
        :tags: interface

        Empty contents displayed when an error occurs navigating to a location
        interactively. Now a warning dialog is displayed and the navigation
        aborted.

        .. seealso:: :meth:`riffle.browser.FilesystemBrowser.setLocation`.

    .. change:: fixed
        :tags: API

        :class:`riffle.model.FilesystemSortProxy` swallowed exceptions
        incorrectly when fetching additional items from the source model.

    .. change:: fixed
        :tags: test

        Interactive test hangs on exit due to second execution loop started.

    .. change:: changed
        :tags: test

        Interactive test now sizes test browser to half of current screen
        dimensions on startup.

    .. change:: new

        Added :ref:`release` to documentation.

.. release:: 0.1.0
    :date: 2014-06-26

    .. change:: new

        Initial release supporting navigation of standard filesystems. Also
        provides support for viewing and navigating file sequences.
